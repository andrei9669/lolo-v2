import React from 'react';

import styles from './index.module.scss';

export function MediaContent({ media, children, className, ...props }) {
	const { medium, url } = media;
	return (
		<div {...props} className={`${styles.mediaContent} ${className || ''}`}>
			<img src={url} alt={medium} />
			{children}
		</div>
	);
}
