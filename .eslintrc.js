module.exports = {
	root: true,
	env: {
		browser: true,
		es6: true,
	},
	parserOptions: {
		ecmaFeatures: {
			jsx: true,
		},
		ecmaVersion: 2018,
		sourceType: 'module',
	},
	parser: 'babel-eslint',
	globals: {
		Atomics: 'readonly',
		SharedArrayBuffer: 'readonly',
	},
	extends: [
		'plugin:react/recommended',
		'react-app',
		'airbnb',
		'plugin:prettier/recommended',
		'prettier/react',
	],
	plugins: ['react', 'react-hooks', 'prettier'],
	rules: {
		'no-plusplus': 0,
		'react/prop-types': 0,
		'react/jsx-indent': 0,
		'prefer-arrow-callback': ['error'],
		'import/prefer-default-export': 0,
		'react/jsx-curly-newline': 0,
		'react-hooks/rules-of-hooks': 'error',
		'react-hooks/exhaustive-deps': 'warn',
		'prettier/prettier': 'error',
		'no-param-reassign': ['error', { props: false }],
		'import/order': [
			'error',
			{
				groups: ['builtin', 'external', 'internal', 'parent', 'sibling', 'index'],
				'newlines-between': 'always-and-inside-groups',
			},
		],
		'react/jsx-wrap-multilines': ['error', { declaration: false, assignment: false }],
	},
};
