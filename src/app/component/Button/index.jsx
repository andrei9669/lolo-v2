import React from 'react';

import styles from './index.module.scss';

export function Button({ children, className, ...props }) {
	return (
		<button {...props} type="button" className={`${styles.button} ${className || ''}`}>
			{children}
		</button>
	);
}
