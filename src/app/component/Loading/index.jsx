import React from 'react';

import styles from './index.module.scss';

export function Loading() {
	return <div className={styles.loader} />;
}
