import React from 'react';

import styles from './index.module.scss';

export function Grid({ children, className }) {
	return <section className={`${styles.grid} ${className || ''}`}>{children}</section>;
}
