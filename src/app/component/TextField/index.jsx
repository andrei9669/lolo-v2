import React from 'react';

import styles from './index.module.scss';

export function TextField({ className, ...props }) {
	return <input className={`${styles.textField} ${className}`} {...props} />;
}
