import { createSlice } from '@reduxjs/toolkit';

const appReducer = createSlice({
	name: 'RSSFeed',
	initialState: {
		RSSFeeds: {
			flipboard: {
				rss: 'https://flipboard.com/@raimoseero/feed-nii8kd0sz.rss',
				active: true,
			},
		},
	},
	reducers: {
		addRSSFeed: (state, { payload }) => {
			state.RSSFeeds[payload.name] = payload;
		},
		editRSSFeed: (state, { payload: { oldName, newName, rss } }) => {
			state.RSSFeeds.some((el) => {
				if (el.name === oldName) {
					el.name = newName;
					el.rss = rss;
					return true;
				}
				return false;
			});
		},
		activateRSSFeed: (state, { payload }) => {
			state.RSSFeeds[payload].active = true;
		},
		deActivateRSSFeed: (state, { payload }) => {
			state.RSSFeeds[payload].active = false;
		},
		removeRSSFeed: (state, { payload }) => {
			delete state.RSSFeeds[payload];
		},
	},
});
export const {
	addRSSFeed,
	removeRSSFeed,
	editRSSFeed,
	activateRSSFeed,
	deActivateRSSFeed,
} = appReducer.actions;
export default appReducer.reducer;
