import versionHandling from './versionHandling';

export const saveState = (state) => {
	try {
		const serializedState = JSON.stringify(state);
		localStorage.setItem('state', serializedState);
		localStorage.setItem('version', '1');
	} catch {
		// ignore write errors
	}
};
export const loadState = () => {
	try {
		const serializedState = localStorage.getItem('state');
		const stateVersion = localStorage.getItem('version');
		if (serializedState === null) {
			return undefined;
		}
		return versionHandling(stateVersion, JSON.parse(serializedState));
	} catch (err) {
		return undefined;
	}
};
