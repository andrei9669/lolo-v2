import React, { useEffect, useState } from 'react';

import { Link } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import RssParser from 'rss-parser';

import { Toolbar } from '../component/Toolbar';
import { getRssFeeds } from '../../redux/selectors/appSelector';
import {
	activateRSSFeed,
	addRSSFeed,
	deActivateRSSFeed,
	editRSSFeed,
	removeRSSFeed,
} from '../../redux/reducers/appReducer';
import {
	Button,
	CardContent,
	Grid,
	Card,
	Table,
	TableBody,
	TableCell,
	TableHead,
	TableRow,
	TextField,
} from '../component';
import buttonStyle from '../component/Button/index.module.scss';

import { Form } from '../component/Form';

import { PROXY } from '../../util/constants';

import styles from './styles/FeedsManager.module.scss';

const parse = new RssParser({
	customFields: {
		item: [['media:content', 'media:content', { keepArray: false }]],
	},
});
const columns = [
	{ id: 'name', value: 'Name' },
	{ id: 'rss', value: 'RSS link' },
];

export default function FeedsManager() {
	const feeds = useSelector(getRssFeeds);
	const dispatch = useDispatch();
	const [inEdit, setInEdit] = useState(null);
	const [values, setValues] = useState({ name: '', rss: '' });
	const [newRow, setNewRow] = useState(true);
	const [mobileView, setMobileView] = useState(false);

	const handleRowChange = ({ target: { value, name } }) => {
		setValues((state) => ({ ...state, [name]: value }));
	};

	const handleSetInEdit = (name, rss) => {
		setNewRow(false);
		setInEdit(name);
		setValues({ name, rss });
	};
	const handleSetNew = () => {
		if (inEdit) {
			setValues({ name: '', rss: '' });
			setInEdit(false);
			setNewRow(true);
		}
	};
	const save = async (e, oldName) => {
		e.preventDefault();
		try {
			await parse.parseURL(PROXY + values.rss);
			dispatch(editRSSFeed({ newName: values.name, rss: values.rss, oldName }));
		} catch (err) {
			console.error(err);
		}
	};
	const addNew = async (e) => {
		e.preventDefault();
		try {
			await parse.parseURL(PROXY + values.rss);
			dispatch(addRSSFeed({ name: values.name, rss: values.rss }));
			setValues({ name: '', rss: '' });
		} catch (err) {
			console.error(err);
		}
	};

	useEffect(() => {
		const resize = () => {
			const width = document.documentElement.clientWidth;
			if (width <= 1000) {
				setMobileView(true);
			} else {
				setMobileView(false);
			}
		};
		resize();
		window.addEventListener('resize', resize);
	}, []);

	return (
		<div
			style={{
				overflow: 'hidden',
			}}
		>
			<Toolbar>
				<div>
					<Link to="/">
						<Button type="button">Return</Button>
					</Link>
				</div>
			</Toolbar>
			<Form
				onSubmit={(e) => {
					e.preventDefault();
				}}
			>
				{mobileView ? (
					<Grid className={styles.grid}>
						{feeds.map(({ name, rss, active }) => (
							<Card key={name}>
								{inEdit === name ? (
									<CardContent>
										{columns.map(({ id, value }) => (
											<label htmlFor={id}>
												{value}
												<TextField name={id} value={values[id]} onChange={handleRowChange} />
											</label>
										))}
										<Button
											type="button"
											className={buttonStyle.buttonSmall}
											onClick={(e) => save(e, name)}
										>
											Save
										</Button>
									</CardContent>
								) : (
									<CardContent onClick={() => handleSetInEdit(name, rss)}>
										<div className={styles.cardContentElements}>
											<span>Name</span>
											<span>{name}</span>
											<span>Rss</span>
											<span>{rss}</span>
											<input
												type="submit"
												value="Remove"
												className={`${buttonStyle.buttonSmall} ${styles.gridAction}`}
												onClick={() => dispatch(removeRSSFeed(name))}
											/>
											{active ? (
												<input
													type="submit"
													value="Deactivate"
													className={`${buttonStyle.buttonSmall} ${styles.gridAction}`}
													onClick={() => dispatch(deActivateRSSFeed(name))}
												/>
											) : (
												<input
													type="submit"
													value="Activate"
													className={`${buttonStyle.buttonSmall} ${styles.gridAction}`}
													onClick={() => dispatch(activateRSSFeed(name))}
												/>
											)}
										</div>
									</CardContent>
								)}
							</Card>
						))}
						<Card>
							<CardContent onClick={handleSetNew}>
								{columns.map(({ id, value }) => (
									<label htmlFor={id} key={id}>
										{value}
										<TextField
											name={id}
											value={newRow ? values[id] : ''}
											onChange={handleRowChange}
										/>
									</label>
								))}
								{!inEdit && (
									<input
										type="submit"
										value="Add"
										className={buttonStyle.buttonSmall}
										onSubmit={addNew}
										onClick={addNew}
									/>
								)}
							</CardContent>
						</Card>
					</Grid>
				) : (
					<Table>
						<TableHead>
							<TableRow>
								{columns.map(({ id, value }) => (
									<TableCell key={id}>{value}</TableCell>
								))}
							</TableRow>
						</TableHead>
						<TableBody>
							{feeds?.map((el) =>
								inEdit === el.name ? (
									<TableRow key={el.name}>
										{columns.map((col) => (
											<TableCell key={col.id} onClick={() => handleSetInEdit(el.name, el.rss)}>
												<TextField
													name={col.id}
													onChange={handleRowChange}
													value={values[col.id]}
												/>
											</TableCell>
										))}
										<TableCell>
											<Button
												type="button"
												className={buttonStyle.buttonSmall}
												onClick={(e) => save(e, el.name)}
											>
												Save
											</Button>
										</TableCell>
									</TableRow>
								) : (
									<TableRow key={el.name}>
										{columns.map((col) => (
											<TableCell
												key={`${el.name}${col.id}`}
												onClick={() => handleSetInEdit(el.name, el.rss)}
											>
												{el[col.id]}
											</TableCell>
										))}
										<TableCell style={{ display: 'flex', flexDirection: 'row' }}>
											<input
												type="submit"
												value="Remove"
												className={buttonStyle.buttonSmall}
												onClick={() => dispatch(removeRSSFeed(el.name))}
											/>
											{el.active ? (
												<input
													type="submit"
													value="Deactivate"
													className={`${buttonStyle.buttonSmall} ${styles.gridAction}`}
													onClick={() => dispatch(deActivateRSSFeed(el.name))}
												/>
											) : (
												<input
													type="submit"
													value="Activate"
													className={`${buttonStyle.buttonSmall} ${styles.gridAction}`}
													onClick={() => dispatch(activateRSSFeed(el.name))}
												/>
											)}
										</TableCell>
									</TableRow>
								),
							)}
							<form>
								<TableRow onClick={handleSetNew}>
									{columns.map((col) => (
										<TableCell key={col.id}>
											<TextField
												name={col.id}
												onChange={handleRowChange}
												value={newRow ? values[col.id] : ''}
											/>
										</TableCell>
									))}
									<TableCell>
										{!inEdit && (
											<input
												type="submit"
												value="Add"
												className={buttonStyle.buttonSmall}
												onSubmit={addNew}
												onClick={addNew}
											/>
										)}
									</TableCell>
								</TableRow>
							</form>
						</TableBody>
					</Table>
				)}
			</Form>
		</div>
	);
}
