import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { Link } from 'react-router-dom';

import RssParser from 'rss-parser';

import { AccountBox, CalendarToday } from '@material-ui/icons';

import { getRssFeeds } from '../../redux/selectors/appSelector';
import { ALL, PROXY, UNCATEGORIZED } from '../../util/constants';
import { formatDate, getCategories, getItemColor } from '../../util';
import { Card, CardContent, CardHeader, CardSubContent } from '../component/Card';

import './index.css';
import cardStyles from '../component/Card/index.module.scss';
import { Button, Toolbar, Grid, Loading, Category } from '../component';
import { MediaContent } from '../component/MediaContent';

const parse = new RssParser({
	customFields: {
		item: [['media:content', 'media:content', { keepArray: false }]],
	},
});

export default function Home() {
	const [displayItems, setDisplayItems] = useState(null);
	const [feed, setFeed] = useState([]);
	const [loading, setLoading] = useState(false);
	const rssFeeds = useSelector(getRssFeeds);
	const [categories, setCategories] = useState([]);
	const [activeCategory, setActiveCategory] = useState(ALL);
	const [renderEndIndex, setRenderEndIndex] = useState(250);

	const renderCategories = () => {
		return categories.map((category) => (
			<Button
				disabled={category === activeCategory}
				onClick={() => setActiveCategory(category)}
				key={category}
			>
				{category}
			</Button>
		));
	};

	useEffect(() => {
		setCategories([ALL, ...getCategories(feed)]);
		const data = feed?.filter((item) => {
			if (activeCategory === ALL) {
				return true;
			}
			return item.category === activeCategory;
		});

		setDisplayItems(data);
	}, [activeCategory, feed]);

	const getFeed = async () => {
		setLoading(true);

		const getFeedName = (rssLink) => {
			return rssFeeds.find(({ rss }) => rss.startsWith(rssLink));
		};

		const cropFeedDescription = (description) => {
			return description && description.length > 400
				? `${description.substring(0, 400)}...`
				: description;
		};

		await (async () => {
			const feeds = [];
			if (rssFeeds) {
				rssFeeds
					.filter((el) => el.active)
					.forEach(({ rss }) => {
						feeds.push(parse.parseURL(PROXY + rss));
					});
			}
			Promise.all(feeds).then((rssData) => {
				const res = [];
				rssData.forEach((feedItems) => {
					const { items, link } = feedItems;
					const feedFromState = getFeedName(link);
					items.forEach((item) => {
						const f = {
							...item,
							author: item.creator,
							color: getItemColor(`${feedFromState?.name}${feedFromState?.rss}`),
							description: cropFeedDescription(item.contentSnippet),
							category: item.categories?.[0]._ ?? UNCATEGORIZED,
						};
						res.push(f);
					});
				});
				res.sort((a, b) => {
					const aDate = a.pubDate ? new Date(a.pubDate) : new Date(a.updated);
					const bDate = b.pubDate ? new Date(b.pubDate) : new Date(b.updated);
					return aDate < bDate ? 1 : -1;
				});
				setFeed(res);
				setLoading(false);
			});
		})();
	};

	useEffect(() => {
		(async () => {
			await getFeed();
		})();

		let start;
		let isScrolling;
		let end;
		const scroll = () => {
			// Set starting position
			if (!start) {
				start = window.pageYOffset;
			}
			// Clear our timeout throughout the scroll
			window.clearTimeout(isScrolling);

			// Set a timeout to run after scrolling ends
			isScrolling = setTimeout(async () => {
				// Calculate distances and run our callback
				end = window.pageYOffset;
				let distance = end - start;
				const maxY = document.documentElement.scrollHeight - document.documentElement.clientHeight;
				if (distance < 0) {
					distance = 100;
					setRenderEndIndex((Math.floor(distance / 10000) + 1) * 200);
					await getFeed();
				} else if (distance >= maxY - 1000) {
					setRenderEndIndex((Math.floor(distance / 10000) + 1) * 200);
				}
			}, 66);
		};
		window.addEventListener('scroll', scroll);
	}, []);

	return (
		<div>
			<Toolbar>
				<div>{renderCategories()}</div>
				<div>
					<Link to="/feeds-manager" style={{ width: 'inherit' }}>
						<Button>Feeds Manager</Button>
					</Link>
				</div>
			</Toolbar>
			<div style={{ marginTop: '120px' }}>
				{loading && (
					<div
						style={{
							width: '100%',
							display: 'flex',
							flexDirection: 'row',
							justifyContent: 'center',
						}}
					>
						<Loading />
					</div>
				)}
				<Grid>
					{displayItems?.slice(0, renderEndIndex).map((item) => {
						let { author } = item;
						const {
							id,
							guid,
							title,
							link,
							pubDate,
							category,
							updated,
							'media:content': mediaContent,
							description,
							color,
						} = item;
						const media = mediaContent?.$ ?? false;
						author = author?.name ?? author;
						const date = pubDate ? formatDate(pubDate) : formatDate(updated);
						return (
							<Card
								key={link || id || guid}
								style={{
									boxShadow: `${color} 0 0.15rem 0.7rem, ${color} 0 0.075rem 0.175rem`,
								}}
							>
								<CardHeader
									className={
										media ? cardStyles.cardHeaderWithImage : cardStyles.cardHeaderWithOutImage
									}
								>
									{media && (
										<Link
											to={{
												pathname: `/article`,
												state: { articleLink: link },
											}}
										>
											<MediaContent media={media} />
										</Link>
									)}
									{category && category !== UNCATEGORIZED && (
										<Category style={{ background: getItemColor(`${category}${category}`) }}>
											{category}
										</Category>
									)}
								</CardHeader>
								<CardContent>
									<Link
										to={{
											pathname: `/article`,
											state: { articleLink: link },
										}}
									>
										<h3>{title}</h3>
									</Link>
									<Link to={{ pathname: `/article`, state: { articleLink: link } }}>
										{description}
									</Link>
									<CardSubContent>
										{author && (
											<p style={{ fontSize: '1rem' }}>
												<AccountBox style={{ marginRight: '1ch', fontSize: '0.8rem' }} />
												{author}
											</p>
										)}
										<p style={{ fontSize: '1rem' }}>
											<CalendarToday
												fontSize="small"
												style={{ marginRight: '1ch', fontSize: '0.8rem' }}
											/>
											{date} ago
										</p>
									</CardSubContent>
								</CardContent>
							</Card>
						);
					})}
				</Grid>
			</div>
		</div>
	);
}
