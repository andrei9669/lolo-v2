import React, { useEffect, useState } from 'react';

import { Menu } from '@material-ui/icons';

import styles from './index.module.scss';

export function Toolbar({ children, className, style }) {
	const [mobileView, setMobileView] = useState(false);
	const [open, setOpen] = useState(false);

	useEffect(() => {
		const resize = () => {
			const height = document.documentElement.clientHeight;
			if (height <= 700) {
				setMobileView(true);
			} else {
				setMobileView(false);
			}
		};
		resize();
		window.addEventListener('resize', resize);
	}, []);
	return (
		<menu className={`${styles.toolbar} ${className}`} style={style} onClick={() => setOpen(!open)}>
			{mobileView && !open && (
				<div
					style={{ height: '7vh', display: 'flex', flexDirection: 'row', justifyContent: 'center' }}
				>
					<Menu fontSize="large" />
				</div>
			)}
			{(!mobileView || open) && children}
		</menu>
	);
}
