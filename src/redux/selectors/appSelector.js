export const getRssFeeds = (state) =>
	Object.entries(state.appReducer.RSSFeeds).reduce((acc, [name, value]) => {
		return [...acc, { name, ...value }];
	}, []);
