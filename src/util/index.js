import { getColor } from 'random-material-color';

import { formatDistanceToNow } from 'date-fns';

import { UNCATEGORIZED } from './constants';

export const getCategories = (items) => {
	if (items) {
		const res = [];
		items.forEach((item) => {
			if (item.categories) {
				item.categories.forEach((category) => {
					if (category._) {
						res.push(category._);
					}
				});
			}
		});

		res.sort();
		res.push(UNCATEGORIZED);
		return [...new Set(res)];
	}
	return [];
};

export const getItemColor = (item) => getColor({ shades: ['400'], text: item });

export const formatDate = (dateString) => {
	const date = new Date(dateString);
	return formatDistanceToNow(date);
};
