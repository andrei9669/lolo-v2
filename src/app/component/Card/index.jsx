import React from 'react';

import styles from './index.module.scss';

export function Card({ children, className, ...props }) {
	return (
		<article {...props} className={`${styles.card} ${className || ''}`}>
			{children}
		</article>
	);
}

export function CardHeader({ children, className, ...props }) {
	return (
		<header {...props} className={`${styles.cardHeader} ${className || ''}`}>
			{children}
		</header>
	);
}

export function CardContent({ children, className, ...props }) {
	return (
		<div {...props} className={`${styles.cardContent} ${className || ''}`}>
			{children}
		</div>
	);
}

export function CardSubContent({ children, className, ...props }) {
	return (
		<div {...props} className={`${styles.cardSubContent} ${className || ''}`}>
			{children}
		</div>
	);
}

export function CardActions({ children, className, ...props }) {
	return (
		<div {...props} className={`${styles.cardActions} ${className || ''}`}>
			{children}
		</div>
	);
}
