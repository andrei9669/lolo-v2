import React from 'react';

import styles from './index.module.scss';

export function Paper({ children, className }) {
	return <div className={`${styles.paper} ${className || ''}`}>{children}</div>;
}
