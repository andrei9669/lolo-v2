import React from 'react';

import { Redirect, useLocation } from 'react-router-dom';

import { IFrameContent } from '../component';

export default function Article() {
	const {
		state: { articleLink },
	} = useLocation();
	return articleLink ? <IFrameContent link={articleLink} /> : <Redirect to="/" />;
}
