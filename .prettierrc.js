module.exports = {
	trailingComma: 'all',
	tabWidth: 2,
	singleQuote: true,
	useTabs: true,
	"jsxBracketSameLine": false,
	arrowParens: "always",
	printWidth:100
};
