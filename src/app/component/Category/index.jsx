import React from 'react';

import styles from './index.module.scss';

export function Category({ children, className, style }) {
	return (
		<div style={style} className={`${styles.category} ${className || ''}`}>
			{children}
		</div>
	);
}
