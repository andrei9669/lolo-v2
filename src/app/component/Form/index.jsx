import React from 'react';

import styles from './index.module.scss';

export function Form({ children, className, ...props }) {
	return (
		<div {...props} className={`${styles.form} ${className || ''}`}>
			{children}
		</div>
	);
}
