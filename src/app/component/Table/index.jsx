import React from 'react';

import styles from './index.module.scss';

export function Table({ children, className, ...props }) {
	return (
		<div className={`${styles.table} ${className || ''}`} {...props}>
			{children}
		</div>
	);
}

export function TableHead({ children, className, ...props }) {
	return (
		<div className={`${styles.thead} ${className || ''}`} {...props}>
			{children}
		</div>
	);
}

export function TableBody({ children, className, ...props }) {
	return (
		<div className={`${styles.tbody} ${className || ''}`} {...props}>
			{children}
		</div>
	);
}

export function TableRow({ children, className, ...props }) {
	return (
		<div className={`${styles.tr} ${className || ''}`} {...props}>
			{children}
		</div>
	);
}

export function TableCell({ children, className, ...props }) {
	return (
		<div className={`${styles.td} ${className || ''}`} {...props}>
			{children}
		</div>
	);
}
