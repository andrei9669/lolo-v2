import React, { useEffect, useState } from 'react';
import Mercury from '@postlight/mercury-parser';

import { Link } from 'react-router-dom';

import { PROXY } from '../../util/constants';

import { Toolbar } from './Toolbar';
import { Button } from './Button';

export function IFrameContent({ link }) {
	const [iFrameContent, setIframeContent] = useState(null);

	useEffect(() => {
		(async () => {
			const data = await Mercury.parse(PROXY + link);
			setIframeContent(data);
		})();
	}, [link]);

	return (
		<div style={{ overflow: 'hidden' }}>
			<Toolbar>
				<Link to="/">
					<Button type="button">Return</Button>
				</Link>
			</Toolbar>
			<div style={{ display: 'flex', justifyContent: 'center', marginTop: '60px' }}>
				<article>
					<h2>{iFrameContent?.title}</h2>
					<span>{iFrameContent?.author}</span>
					<span>{iFrameContent?.date_published}</span>
					<div
						style={{ maxWidth: '700px' }}
						dangerouslySetInnerHTML={{ __html: iFrameContent?.content }}
					/>
				</article>
			</div>
		</div>
	);
}
