import thunkMiddleware from 'redux-thunk';

import { configureStore } from '@reduxjs/toolkit';

import rootReducer from './reducers';
import { loadState, saveState } from './util/storage';

const store = configureStore({
	reducer: rootReducer,
	middleware: [thunkMiddleware],
	preloadedState: loadState(),
});

store.subscribe(() => {
	saveState({ appReducer: { RSSFeeds: store.getState().appReducer.RSSFeeds } });
});

export default store;
