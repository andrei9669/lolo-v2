import React from 'react';

import { Route, Switch } from 'react-router-dom';

import Home from './pages/Home';
import Article from './pages/Article';
import FeedsManager from './pages/FeedsManager';

export default function App() {
	return (
		<Switch>
			<Route exact path="/">
				<Home />
			</Route>
			<Route exact path="/feeds-manager">
				<FeedsManager />
			</Route>
			<Route path="/article">
				<Article />
			</Route>
			<Route>
				<Home />
			</Route>
		</Switch>
	);
}
